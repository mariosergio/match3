﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerDataReader : MonoBehaviour {

	public InputField playerNameInputField;
	public Dropdown playerSexDropdown;

	private void Start () {
		if ((GameData.data != null) && (GameData.data.playerName != "")) {
			playerNameInputField.text = GameData.data.playerName;
			playerSexDropdown.value = (int)GameData.data.playerSex;
		}
	}

	public void ReadPlayerData () {
		GameData.data.playerName = playerNameInputField.text;
		GameData.data.playerSex = (Sex)playerSexDropdown.value;
	}

	public void StartGame (int mode) {
		GameMode gameMode = (GameMode)mode;

		GameData.data.currentMode = gameMode;

		SceneManager.LoadScene (GameData.data.GetGameScene (gameMode));
	}
}
