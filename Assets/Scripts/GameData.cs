﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameMode {
	Color,
	Forms
}

public enum Sex {
	M,
	F
}

public struct MatchData {
	public bool matchPlayed;
	public int matchesCount;
}

public class GameData : MonoBehaviour {

	public static GameData data;

	public GameMode currentMode;

	public string playerName;
	public Sex playerSex;
	public GameMode startingMode;

	public MatchData colorModeData;
	public MatchData formsModeData;

	// Use this for initialization
	private void Start () {
		if (data != null)
			Destroy (gameObject);
		else {
			DontDestroyOnLoad (gameObject);
			data = this;

			InitializeData ();
		}
	}

	private void InitializeData () {
		currentMode = GameMode.Color;

		playerName = "";
		startingMode = GameMode.Color;

		colorModeData = new MatchData ();
		colorModeData.matchPlayed = false;
		colorModeData.matchesCount = 0;

		formsModeData = new MatchData ();
		formsModeData.matchPlayed = false;
		formsModeData.matchesCount = 0;
	}

	public void StartGame (GameMode gameMode) {
		currentMode = gameMode;
	}

	public void AddMatchCount () {
		switch (currentMode) {
		case GameMode.Color:
			colorModeData.matchesCount++;
			break;

		case GameMode.Forms:
			formsModeData.matchesCount++;
			break;

		default:
			Debug.LogError ("No match being played!");
			break;
		}
	}

	public void EndGame () {

		switch (currentMode) {
		case GameMode.Color:
			SaveColorGameData ();
			
			break;

		case GameMode.Forms:
			SaveFormsGameData ();

			break;
		}
	}

	public void RestartGame () {
		SceneManager.LoadScene ("startingMenu");
	}

	public string GetGameScene (GameMode mode) {
		string sceneName = "startingMenu";

		switch (mode) {
		case GameMode.Color:
			sceneName = "colorGame";
			break;

		case GameMode.Forms:
			sceneName = "formsGame";
			break;
		}

		return sceneName;
	}

	private void SaveColorGameData () {
		// write game data on file
		string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/gameData_color.csv";
		// This text is added only once to the file.
		if (!File.Exists(path)) 
		{
			// Create a file to write to.
			using (StreamWriter sw = File.CreateText(path)) 
			{
				sw.WriteLine("horario;nome;sexo;quantidade de combinacoes");
			}
		}

		string gameDataText = "";
		gameDataText += DateTime.Now.ToLongTimeString () + ";";
		gameDataText += playerName + ";";
		gameDataText += playerSex.ToString () + ";";
		//gameDataText += startingMode.ToString () + ";";

		gameDataText += colorModeData.matchesCount;

		//gameDataText += formsModeData.matchesCount;

		// This text is always added, making the file longer over time
		// if it is not deleted.
		using (StreamWriter sw = File.AppendText(path)) 
		{
			sw.WriteLine(gameDataText);
		}
	}

	private void SaveFormsGameData () {
		// write game data on file
		string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/gameData_forms.csv";
		// This text is added only once to the file.
		if (!File.Exists(path)) 
		{
			// Create a file to write to.
			using (StreamWriter sw = File.CreateText(path)) 
			{
				sw.WriteLine("horario;nome;sexo;quantidade de combinacoes");
			}
		}

		string gameDataText = "";
		gameDataText += DateTime.Now.ToLongTimeString () + ";";
		gameDataText += playerName + ";";
		gameDataText += playerSex.ToString () + ";";
		//gameDataText += startingMode.ToString () + ";";

		//gameDataText += colorModeData.matchesCount;

		gameDataText += formsModeData.matchesCount;

		// This text is always added, making the file longer over time
		// if it is not deleted.
		using (StreamWriter sw = File.AppendText(path)) 
		{
			sw.WriteLine(gameDataText);
		}
	}
}
