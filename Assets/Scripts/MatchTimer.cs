﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchTimer : MonoBehaviour {

	public float matchTimeLimit = 30;
	private float currentMatchDuration;

	private bool matchStarted;

	public Text timerText;
	public GameObject gameOverPanel;

	// Use this for initialization
	public void StartTimer () {
		matchStarted = true;
		currentMatchDuration = matchTimeLimit;
	}
	
	// Update is called once per frame
	private void Update () {
		if (!matchStarted)
			return;

		currentMatchDuration = Mathf.Clamp(currentMatchDuration - Time.deltaTime, 0f, matchTimeLimit);

		timerText.text = currentMatchDuration.ToString ("#00.00");

		if (currentMatchDuration <= 0f) {
			matchStarted = false;

			FindObjectOfType<ShapesManager> ().DestroyAllCandy ();
			gameOverPanel.SetActive (true);
			GameData.data.EndGame ();
		}
	}
}
